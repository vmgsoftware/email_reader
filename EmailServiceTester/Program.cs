﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Email_Reader;
using Email_Reader.Services;
using Newtonsoft.Json;

namespace EmailServiceTester
{
    class Program
    {
        static void Main(string[] args)
        {

            var a = new Program();

            Task.WaitAll(a.RunLeadsLoop());

            // var jsonData = d.First().ToJsonData();
            //
            // var result = magic.ProcessDataAutoTraderApi(jsonData);

            Console.WriteLine("Press any key.");
            Console.ReadKey();
        }

        public async Task RunLeadsLoop()
        {
            var quit = false;

            while (!quit)
            {
                Console.WriteLine(@"Downloading new leads.");

                var db = new AutoTraderLeadsBackupDatabase();

                var lastUpdated = db.GetLastUpdatedDate().AddDays(-1);
                var currentTime = DateTime.Now;

                var a = new AutoTraderLeadsApi(new Uri("https://services.autotrader.co.za/api/lead/v1.0"),
                    "vmg@autotrader.co.za",
                    "PsOzhfY72ZLKYkI8nHjI");

                var d = a.DownloadLeads(lastUpdated, currentTime);

                // foreach (var autoTraderLead in d)
                // {
                //     Console.WriteLine(d.WriterProperties());
                // }

                var json = JsonConvert.SerializeObject(d);
                Console.WriteLine(json);


                if (d.Any())
                {
                    Console.WriteLine($@"Downloaded {d.Count} leads from {lastUpdated} to {currentTime}.");
                    db.SaveAutoTraderLeads(d);
                }
                else
                {
                    Console.WriteLine($@"Downloaded 0 leads from {lastUpdated} to {currentTime}.");
                }

                db.SetLastUpdatedDate(currentTime);

                var leads = db.GetLeadsForProcessing();

                Console.WriteLine($@"{leads.Count} waiting for processing");

                foreach (var lead in leads)
                {
                    try
                    {
                        var leadJson = lead.ToJsonData();
                        db.SetLeadStatus(lead.Id, AutoTraderLeadStatus.Processed);
                        db.WriteDbLog(lead.Id, "PROCESSED");
                    }
                    catch
                    {
                        db.SetLeadStatus(lead.Id, AutoTraderLeadStatus.Failed);
                        db.WriteDbLog(lead.Id, "FAILED");
                    }
                    
                }

                Console.WriteLine(@"Waiting for 5 minutes.");
                if (Console.KeyAvailable) quit = true;
                await Task.Delay(TimeSpan.FromMinutes(5));
            }
        }
    }

    public static class PropertyWriter
    {
        public static string WriterProperties(this Object obj)
        {
            var props = obj.GetType().GetProperties(BindingFlags.Public| BindingFlags.Instance);

            var names = props.Aggregate(new StringBuilder(), (sb, p) => sb.Append(p.GetValue(obj)));

            return names.ToString();
        }
    }
}
