﻿Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading
Imports Email_Reader.Utilities

Public Class srvEmailReader
    Private trd As Thread
    Public myTimer As New Timers.Timer
    



    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        WriteErrorLog("Email Reader Started")

        myTimer.Interval = 120000
        'AddHandler myTimer.Elapsed, AddressOf send_notification
        AddHandler myTimer.Elapsed, AddressOf Create_Thread
        myTimer.Start()
        myTimer.Enabled = True

        Dim mail As New MailMessage
        mail.To.Add("stephen@vmgsoftware.co.za")
'        mail.To.Add("steven@vmgsoftware.co.za")
        mail.Subject = "Email_Reader Error"
        mail.IsBodyHtml = True

        mail.Body = "Email_Reader Service has started"

        EmailService.SendMail(mail)
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        WriteErrorLog("Email_Reader stopped")

        If IsNothing(trd) = False And trd.IsAlive Then
            trd.Interrupt()
            trd.Abort()
        End If
        myTimer.Stop()
        myTimer.Enabled = False
        Dim mail As New MailMessage
        mail.To.Add("stephen@vmgsoftware.co.za")
'        mail.To.Add("steven@vmgsoftware.co.za")
        mail.Subject = "Email_Reader Error"
        Mail.IsBodyHtml = True

        Mail.Body = "Email_Reader Service has stopped"

        EmailService.SendMail(Mail)
        Environment.Exit(0)
    End Sub


    Sub Create_Thread()
        If IsNothing(trd) OrElse Not trd.IsAlive Then
            trd = New Thread(AddressOf tick_tock)
            trd.Priority = ThreadPriority.Normal
            trd.IsBackground = True
            trd.Start()
        End If
    End Sub
End Class

