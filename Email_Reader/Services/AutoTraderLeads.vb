﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.Text
Imports Dapper
Imports Dapper.Contrib.Extensions
Imports Newtonsoft.Json
Imports RestSharp

Namespace Services
    Public Class AutoTraderLeadsApi
        Private ReadOnly _endpointUrl As Uri
        Private ReadOnly _username As String
        Private ReadOnly _password As String

        Public Sub New(endpointUrl As Uri, username As String, password As String)
            _endpointUrl = endpointUrl
            _username = username
            _password = password
        End Sub

        Private Function GetAuthorisation() As String
            Dim authString = $"{_username}:{_password}"
            Dim plainTestBytes = Encoding.UTF8.GetBytes(authString)
            return Convert.ToBase64String(plainTestBytes)
        End Function

        Public Function DownloadLeads(fromDate As DateTime, toDate As DateTime) As List(Of AutoTraderLead)
            Dim restClient = new RestClient(_endpointUrl)
            Dim request = new RestRequest("leads", Method.GET)
            request.AddHeader("Authorization", $"Basic {GetAuthorisation()}")
            request.AddOrUpdateParameter("from", fromDate.ToString("yyyy-MM-dd HH:mm:ss"))
            request.AddOrUpdateParameter("to", toDate.ToString("yyyy-MM-dd HH:mm:ss"))
            Dim response = restClient.Execute(request)
            If (response.StatusCode = HttpStatusCode.OK)
                Dim downloadedLeads = JsonConvert.DeserializeObject(Of AutoTraderLeadWrapper)(response.Content)
                Return downloadedLeads.SendContactMessageLeads
            End If
            Return Nothing
        End Function
    End Class

    Public Class AutoTraderLeadWrapper
        Property SendContactMessageLeads() As List(Of AutoTraderLead)
    End Class

    <Dapper.Contrib.Extensions.Table("tblAutoTraderLeadDownloads")>
    Public Class AutoTraderLead
        <ExplicitKey>
        Property Id() As Long
        Property ListingId() As Long
        Property DealerId() As Long
        Property StockNumber() As String
        Property Name() As String
        Property PhoneNumber() As String
        Property EmailAddress() As String
        Property Message() As String
        <JsonProperty(propertyName := "date")>
        Property LeadDate() As String
        Property NewUsed() As String
        Property Make() As String
        Property Model() As String
        <JsonProperty(propertyName := "variant")>
        Property VehicleVariant() As String
        Property Mileage() As Decimal
        Property RegistrationYear() As Integer
        Property Price() As Decimal
        Property RegistrationNumber() As String
        Public Function ToJsonData() As JsonData
            Dim data = new JsonData()
            data.Name = me.Name
            data.Cellphone_no = me.PhoneNumber
            data.Email = me.EmailAddress
            data.comment = me.Message
            data.did = DealerId
            data.make = Make
            data.mileage = Mileage
            data.model = Model + " " + IIf(String.IsNullOrEmpty(VehicleVariant), "", VehicleVariant)
            data.price = Price
            data.reference = ListingId
            data.reg = StockNumber
            data.year = RegistrationYear
            Return data
        End Function

    End Class

    Public Class AutoTraderLeadsBackupDatabase
        Private ReadOnly _connection As String

        Public Sub New(optional connection As String = Nothing)
            If String.IsNullOrEmpty(connection)
                _connection = "data source=db4.vmgsoftware.co.za,50648;initial catalog=email_reader;persist security info=False;packet size=4096;user id = email_user;password = :WXQ_DlxO=e}"
            Else 
                _connection = connection
            End If
        End Sub

        Public Sub OpenConnection()
            
        End Sub

        Public Sub InsertAutoTraderLead(ctx As SqlConnection, lead As AutoTraderLead)
            If LeadExists(ctx, lead.Id) Then Return
            dim newLeadDate = DateTime.Parse(lead.LeadDate)

            ctx.Execute("insert into tblAutoTraderLeadDownloads (id, ListingId, DealerId, StockNumber, Name, PhoneNumber, EmailAddress, Message, LeadDate, NewUsed, Make, Model, VehicleVariant, Mileage, RegistrationYear, Price, RegistrationNumber, Processed) values
(@id, @ListingId, @DealerId, @StockNumber, @Name, @PhoneNumber, @EmailAddress, @Message, @LeadDate, @NewUsed, @Make, @Model, @VehicleVariant, @Mileage, @RegistrationYear, @Price, @RegistrationNumber, @Processed)", New With {
                           .id = lead.Id, 
                           .ListingId = lead.ListingId, 
                           .DealerId = lead.DealerId, 
                           .StockNumber = lead.StockNumber, 
                           .Name = lead.Name, 
                           .PhoneNumber = lead.PhoneNumber, 
                           .EmailAddress = lead.EmailAddress, 
                           .Message = lead.Message,
                           .LeadDate = newLeadDate, 
                           .NewUsed = lead.NewUsed, 
                           .Make = lead.Make, 
                           .Model = lead.Model, 
                           .VehicleVariant = lead.VehicleVariant, 
                           .Mileage = lead.Mileage, 
                           .RegistrationYear = lead.RegistrationYear, 
                           .Price = lead.Price, 
                           .RegistrationNumber = lead.RegistrationNumber, 
                           .Processed = 0
                           })
        End Sub

        Public Sub SaveAutoTraderLead(lead As AutoTraderLead)
            Using ctx As New SqlConnection(_connection)
                Try
                    InsertAutoTraderLead(ctx, lead)
                Catch ex As Exception
                    WriteErrorLog($"Failed to insert downloaded lead id {lead.ListingId}.")
                    WriteErrorLog(ex)
                End Try
            End Using
        End Sub

        Public Sub SaveAutoTraderLeads(leads As List(of AutoTraderLead))
            Using ctx As New SqlConnection(_connection)
                For Each lead As AutoTraderLead In leads
                    Try
                        InsertAutoTraderLead(ctx, lead)
                    Catch ex As Exception
                        WriteErrorLog($"Failed to insert downloaded lead id {lead.ListingId}.")
                        WriteErrorLog(ex)
                    End Try
                Next
            End Using
        End Sub

        Public Function GetLastUpdatedDate() As DateTime
            Using ctx As New SqlConnection(_connection)
                Dim lastUpdated = ctx.ExecuteScalar(Of DateTime)("select LastRun from tblAutoTraderLeadsSettings where id = 1")
                Return lastUpdated
            End Using
        End Function

        Public Sub SetLastUpdatedDate(newTime As DateTime)
            Using ctx As New SqlConnection(_connection)
                ctx.ExecuteScalar("update tblAutoTraderLeadsSettings set LastRun = @lastUpdated where id = 1", new With {.lastUpdated = newTime})
            End Using
        End Sub

        Private Function LeadExists(ctx As SqlConnection, leadId As Integer) As Boolean
            Dim result = ctx.ExecuteScalar(of Integer)("select count(*) from tblAutoTraderLeadDownloads where id = @leadId", New With {
                                                          .leadId = leadId
                                                          })
            Return result > 0 
        End Function

        Public Sub SetLeadStatus(leadId As Long, status As AutoTraderLeadStatus)
            Dim newStatus = CInt(status)
            Using ctx As New SqlConnection(_connection)
                ctx.ExecuteScalar("update tblAutoTraderLeadDownloads set Processed = @newStatus where id = @leadId", new With {.leadId = leadId, .newStatus = newStatus})
            End Using
        End Sub

        Public Function GetLeadsForProcessing() As IList(Of AutoTraderLead)
            Using ctx As New SqlConnection(_connection)
                Dim leads = ctx.Query(Of AutoTraderLead)("select * from tblAutoTraderLeadDownloads where processed = @processedFlag", new with {.processedFlag = CInt(AutoTraderLeadStatus.Unprocessed)})
                Return leads
            End Using
        End Function

        Public Sub WriteDbLog(leadId As Long, message As string)
            Using ctx As New SqlConnection(_connection)
                ctx.Execute("insert into tblAutoTraderLeadLogs(leadId, logDate, description) values (@leadId, @logDate, @description)", New With 
                               {
                               .leadId = leadId,
                               .logDate = DateTime.Now,
                               .description = message })
            End Using
        End Sub

    End Class

    Public Enum AutoTraderLeadStatus
        Unprocessed = 0
        Processed = 1
        Failed = 3
    End Enum

End NameSpace