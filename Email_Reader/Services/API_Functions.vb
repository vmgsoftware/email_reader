﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports Email_Reader.Services
Imports RestSharp

Module API_Functions
    Public Function GetToken() As String

        Try

        
            Dim status As HttpStatusCode = HttpStatusCode.ExpectationFailed
            Dim content As String = ""
            Dim larray() As String

            'content = "{ ""username"":  """ & txtUser.Text & """, ""password"": """ & txtpwd.Text & """,  ""client_id"": ""HLATuHH7KaQNO3VFl0AH99JNJiN1nYTw"",  ""connection"": ""config-user-db"",  ""scope"": ""openid role branch_guid"",  ""grant_type"": ""password""}"
            content = "{ ""username"":  ""stephen@vmgsoftware.co.za"", ""password"": ""KpFVTkyChDbmsCP<zF7v"",  ""client_id"": ""HLATuHH7KaQNO3VFl0AH99JNJiN1nYTw"",  ""connection"": ""config-user-db"",  ""scope"": ""openid role"",  ""grant_type"": ""password""}"
            Dim response As Byte() = PostResponse("https://vmg.eu.auth0.com/oauth/ro", content, status, 1)
            Dim responseString As String = ""
            If response IsNot Nothing Then

                larray = Split(System.Text.Encoding.UTF8.GetString(response), ",")
                For lX = 0 To UBound(larray)
                    If larray(lX).Contains("{""id_token"":""") Then
                        responseString = Replace(larray(lX), "{""id_token"":""", "Bearer ")
                        responseString = Replace(responseString, """", "")
                    End If
                Next

                GetToken = responseString
            Else
                GetToken = ""
            End If
        Catch ex As Exception
            WriteErrorLog(ex)
            GetToken = ""
        End Try
    End Function

    Public Function PostResponse(url As String, content As String, ByRef statusCode As HttpStatusCode, type As Integer, optional token As String = "") As Byte()
        'type 1 = getting token
        'type 2 = posting to leads
        Dim fails As Integer = 0
try_post_again:
        Dim responseFromServer As Byte() = Nothing
        Dim dataStream As Stream = Nothing
        Try
            Dim request As WebRequest = WebRequest.Create(url)
            request.Timeout = 120000
            request.Method = "POST"
            If type = 2 Then
                ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)
                request.Headers("Authorization") = token
            End If
            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(content)
            request.ContentType = "application/json"
            request.ContentLength = byteArray.Length
            'System.Threading.thread.sleep(1000) 
            dataStream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Dim response As WebResponse = request.GetResponse()
            dataStream = response.GetResponseStream()
            Dim ms As New MemoryStream()
            Dim thisRead As Integer = 0
            Dim buff As Byte() = New Byte(1023) {}
            Do
                thisRead = dataStream.Read(buff, 0, buff.Length)
                If thisRead = 0 Then
                    Exit Do
                End If
                ms.Write(buff, 0, thisRead)
            Loop While True

            If type = 1 Then
                responseFromServer = ms.ToArray()
                dataStream.Close()
                response.Close()
                statusCode = HttpStatusCode.OK
            ElseIf type = 2 Then
                statusCode = DirectCast(response, HttpWebResponse).StatusCode
                responseFromServer = Encoding.ASCII.GetBytes(statusCode.ToString)
            End If
        Catch ex As WebException
            If ex.Response IsNot Nothing Then
                dataStream = ex.Response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)
                Dim resp As String = reader.ReadToEnd()
                statusCode = DirectCast(ex.Response, HttpWebResponse).StatusCode
               ' txtresponse.Text = "Code : " & statusCode & vbCrLf & "Description : " & resp
            Else
                Dim resp As String = ""
                statusCode = HttpStatusCode.ExpectationFailed
                if fails = 0 then
                    fails = 1 
                    Goto try_post_again
                End If
            End If
        Catch ex As Exception
            statusCode = HttpStatusCode.ExpectationFailed
        End Try
        Return responseFromServer

    End Function

    public Function send_data_API(data As JsonData, company_id As integer , current_token as string)As String
        try
            Dim status As HttpStatusCode = HttpStatusCode.ExpectationFailed
            Dim content As String = ""
            Dim responseString As String = ""
            dim msg as String =  data.comment & vbCrLf & "Web Reference: " & data.reference

            content = "{""dealer_id"": """ & company_id & """,""lead_name"":  """ & data.Name & """, ""cellphone_no"": """ & data.Cellphone_no & """,  ""email_address"": """ & data.Email & """,  ""lead_source"": ""Autotrader"",""selling"": ""False"", ""stock_id"":""0"",""make"": """ & data.make & """, ""model_desc"": """ & data.model & """, ""mileage"": """ & data.mileage & """, ""price"": """ & data.price & """, ""stock_code"": """ & data.reg & """, ""message"": """ & msg & """, ""year"": """ & data.year & """}"
            Dim response As Byte() = PostResponse("https://api.vmgdms.com/leads/v1/new_dealer_lead", content, status, 2,current_token)

            If response IsNot Nothing Then
                responseString = System.Text.Encoding.UTF8.GetString(response)
            Else 
                responseString=""
            End If

            Return responseString

        Catch ex As Exception
            
            WriteErrorLog(ex)
            Return ""
        End Try
    End Function

    ' AutoTrader 2
    public Function send_data_API(data As JsonDataAutoTrader2, company_id As integer , current_token as string)As String
        try
            Dim status As HttpStatusCode = HttpStatusCode.ExpectationFailed
            Dim content As String = ""
            Dim responseString As String = ""
            dim msg as String =  data.comment & vbCrLf & "Web Reference: " & data.reference

            content = "{""dealer_id"": """ & company_id & """,""lead_name"":  """ & data.Name & """, ""cellphone_no"": """ & data.Cellphone_no & """,  ""email_address"": """ & data.Email & """,  ""lead_source"": ""Autotrader"",""selling"": ""False"", ""stock_id"":""0"", ""price"": """ & data.price & """, ""message"": """ & data.vehicle + ": " + msg & """}"
            Dim response As Byte() = PostResponse("https://api.vmgdms.com/leads/v1/new_dealer_lead", content, status, 2,current_token)

            If response IsNot Nothing Then
                responseString = System.Text.Encoding.UTF8.GetString(response)
            Else 
                responseString=""
            End If

            Return responseString

        Catch ex As Exception
            
            WriteErrorLog(ex)
            Return ""
        End Try
    End Function

    public Function send_data_API(data As IxJsonData, company_id As integer , current_token as string)As String
        Dim status As HttpStatusCode = HttpStatusCode.ExpectationFailed
        Dim content As String = ""
        Dim responseString As String = ""
        dim msg as String =  data.comment & vbCrLf & "Web Reference: " & data.iX_Lead_Ref
       ' dim calc_year as String = Trim(Left(data.comment,5))
        'dim string_left as String = Trim(right(data.comment,Len(data.comment)-5))
        dim make as String  = Trim(Left(data.comment, InStr(data.comment, " ")))
        'content = "{""dealer_id"": """ & company_id & """,""lead_name"":  """ & data.Name & """, ""cellphone_no"": """ & data.Cellphone_no.Replace(" ","") & """,  ""email_address"": """ & data.Email & """,  ""lead_source"": ""Website_IX"",""selling"": ""False"", ""stock_id"":""0"", ""price"": """ & data.price & """, ""stock_code"": """ & data.stock_code & """, ""message"": """ & msg & """, ""make"": """ & make & """, ""model_desc"": """", ""year"": """ & calc_year & """}"
        content = "{""dealer_id"": """ & company_id & """,""lead_name"":  """ & data.Name & """, ""cellphone_no"": """ & data.Cellphone_no.Replace(" ","") & """,  ""email_address"": """ & data.Email & """,  ""lead_source"": ""Website_IX"",""selling"": ""False"", ""stock_id"":""0"",  ""stock_code"": """ & data.stock_code & """, ""message"": """ & msg & """, ""make"": """ & make & """, ""model_desc"": """"}"

        Dim response As Byte() = PostResponse("https://api.vmgdms.com/leads/v1/new_dealer_lead", content, status, 2,current_token)

        If response IsNot Nothing Then
            responseString = System.Text.Encoding.UTF8.GetString(response)
        Else 
            responseString=""
        End If

        WriteErrorLog("IX reference: " & data.iX_Lead_Ref & " --> " & responseString)
        Return responseString
    End Function

    public Function send_data_API(data As BPFMC_JSON_data, company_id As integer , current_token as string)As String
        Dim status As HttpStatusCode = HttpStatusCode.ExpectationFailed
        Dim content As String = ""
        Dim responseString As String = ""
        dim msg as String =  data.comment
        ' dim calc_year as String = Trim(Left(data.comment,5))
        'dim string_left as String = Trim(right(data.comment,Len(data.comment)-5))
        dim make as String  = Trim(Left(data.comment, InStr(data.comment, " ")))
        'content = "{""dealer_id"": """ & company_id & """,""lead_name"":  """ & data.Name & """, ""cellphone_no"": """ & data.Cellphone_no.Replace(" ","") & """,  ""email_address"": """ & data.Email & """,  ""lead_source"": ""Website_IX"",""selling"": ""False"", ""stock_id"":""0"", ""price"": """ & data.price & """, ""stock_code"": """ & data.stock_code & """, ""message"": """ & msg & """, ""make"": """ & make & """, ""model_desc"": """", ""year"": """ & calc_year & """}"
        content = "{""dealer_id"": """ & company_id & """,""lead_name"":  """ & data.Name & " "  &  data.Surname & """, ""cellphone_no"": """ & data.Cellphone_no.Replace(" ","") & """,  ""email_address"": """ & data.Email & """,  ""lead_source"": """ & data.lead_source & """,""selling"": ""False"", ""stock_id"":""0"",  ""stock_code"": """ & data.stock_code & """, ""message"": """ & msg & """,""referral_url"": """ & data.referral_url & """, ""colour"": """ & data.colour & """, ""year"": """ & data.year & """,""price"": """ & data.price & """,""mileage"": """ & data.mileage.Replace("km","") & """, ""make"": """ & data.make & """, ""model"": """ & data.model & """}"

        Dim response As Byte() = PostResponse("https://api.vmgdms.com/leads/v1/new_dealer_lead", content, status, 2,current_token)

        If response IsNot Nothing Then
            responseString = System.Text.Encoding.UTF8.GetString(response)
        Else 
            responseString=""
        End If

        WriteErrorLog("BPFMC : " & data.Cellphone_no & " --> " & responseString)
        Return responseString
    End Function
End Module
