﻿Imports System.IO
Imports System.Net
Imports HtmlAgilityPack
Imports MailKit.Net.Smtp
Imports MailKit.Security
Imports MimeKit
Imports MimeKit.Utils
Imports Newtonsoft.Json


Module IX_Functions
    Public Function process_data(data As IxJsonData, connection As String) As String
        Dim compid As Integer = 0
        Dim localDb = New MssqlDatabaseHelper(connection)
        Dim ldata As New DataTable
        Dim result As String = ""
        ldata = (localDb.ExecuteDataSet("select * from tbldid where IX_ID = '" & data.dealer & "' and deleted = 0", "did_table")).Tables("did_table")

        If ldata.Rows.Count > 0 Then
            For Each dataRow As DataRow In ldata.Rows
                compid = CInt(dataRow.Item("company_id").ToString())
            Next

            If compid <> 0 Then
                Dim my_token As String = GetToken
                result = send_data_API(data, compid, my_token)
            Else
                result = ""
            End If
        End If

        Return result
    End Function
    Public Sub clean_text_IX(a_nodes As List(Of HtmlNode), data As IxJsonData)
        Console.WriteLine()
        Dim long_str As String = ""
        Dim new_list As New List(Of String)
        For Each a_node In a_nodes
            ' Console.WriteLine("TEXT: {0}",a_node.InnerText.Trim())
            If a_node.InnerText.Trim() <> "" Then
                if a_node.InnerText <> "&nbsp;" then
                    new_list.Add(a_node.InnerText.Trim().ToString())
                    Console.WriteLine("TEXT: {0}",a_node.InnerText.Trim())
                End If
            End If
        Next


        For i = 0 To new_list.Count - 1
            'Console.WriteLine(new_list(i))
            Select Case new_list(i)
                Case "Prospect"
                    data.Name = new_list(i + 1)
                Case "Mobile"
                    data.Cellphone_no = new_list(i + 1)
                Case "Email"
                    data.Email = new_list(i + 1)
                Case "Stock #"
                    data.stock_code = new_list(i + 1)
                Case "Listed Price"
                    data.price = new_list(i + 1).Replace(" ", "")
                Case "Registration Number"
                    data.reg = new_list(i + 1)
                Case "Dealer"
                    data.dealer = new_list(i + 1)
                Case "iX Lead Ref"
                    data.iX_Lead_Ref = new_list(i + 1)
                Case "Source"
                    data.lead_source = new_list(i + 1)
                Case "Source ref"
                    data.lead_ref = new_list(i + 1)
                Case "Vehicle"
                    data.comment = new_list(i + 1)
                Case Else
                    If new_list(i).Contains("Used Vehicle:") Then
                        data.comment = new_list(i).Replace("Used Vehicle:", "")
                    End If
            End Select

        Next

        Console.WriteLine(JsonConvert.SerializeObject(data))

    End Sub

    Public Function getEmailAddress(data As IxJsonData, connection As String) As String

        Dim localDb = New MssqlDatabaseHelper(connection)
        Dim ldata As New DataTable
        Dim result As String = ""
        ldata = (localDb.ExecuteDataSet("select * from tbldid where IX_ID = '" & data.dealer & "' and deleted = 0", "did_table")).Tables("did_table")

        If ldata.Rows.Count > 0 Then
            For Each dataRow As DataRow In ldata.Rows
                result = dataRow.Item("IX_FnI_email").ToString()
            Next
        End If

        Return result
    End Function

    public function forward_email(messageToForward As MimeMessage , email_address As string, email_type As Integer) As string

        Try
            if email_address <> "" then
               
                dim email_from_desc As string = ""

                if email_type = 1 then 
                    email_from_desc = "F&I Lead on behalf of iX"
                ElseIf  email_type = 2 then
                    email_from_desc = "General Enquiry"
                ElseIf  email_type = 3 then
                    email_from_desc = "Sell my car"
                End If

                dim message = new MimeMessage
                message.From.Add(New MailboxAddress(email_from_desc,"emailreader@vmgsoftware.co.za"))
                message.ReplyTo.Add(New MailboxAddress(email_from_desc,"emailreader@vmgsoftware.co.za"))
                message.To.Add(New mailboxaddress(email_address,email_address))

                Dim builder = new BodyBuilder
                builder.TextBody = "Please see attached email from your iX Website." & vbCrLf & "Forwarded from VMG CRM." & vbCrLf & vbCrLf & "Have a great day!" & vbCrLf &vbCrLf & "Regards Team VMG"
                builder.Attachments.Add(new MessagePart With {.Message = messageToForward})
                
                message.Body = builder.ToMessageBody()

                using client = New SmtpClient
                    client.Connect("154.66.196.109", 25, SecureSocketOptions.None)
                    client.Authenticate("emailreader@vmgsoftware.co.za", "123Ema+321")

                    client.Send(message)
                    client.Disconnect(True)
                End Using


                forward_email = "Success"
            Else 
                forward_email = "Fail"    
            end if
        Catch ex As Exception
            WriteErrorLog("forward_email : " & ex.ToString())
            forward_email ="Fail"
        End Try
    End function
End Module
