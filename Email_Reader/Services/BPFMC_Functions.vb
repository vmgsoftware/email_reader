﻿Imports HtmlAgilityPack
Imports Newtonsoft.Json

Module BPFMC_Functions
     Public Sub clean_text_BPFMC(a_nodes As List(Of HtmlNode), data As BPFMC_JSON_data)
        Console.WriteLine()
        Dim long_str As String = ""
        Dim new_list As New List(Of String)
        For Each a_node In a_nodes
            ' Console.WriteLine("TEXT: {0}",a_node.InnerText.Trim())
            If a_node.InnerText.Trim() <> "" Then
                if a_node.InnerText <> "&nbsp;" then
                    new_list.Add(a_node.InnerText.Trim().ToString())
                    Console.WriteLine("TEXT: {0}",a_node.InnerText.Trim())
                End If
            End If
        Next


         For i = 0 To new_list.Count - 1
             'Console.WriteLine(new_list(i))
             Select Case new_list(i)
                 Case "Name:"
                     data.Name = new_list(i + 1)
                 Case "Surname:"
                     data.Surname= new_list(i + 1)
                 Case "Email:"
                     data.Email = new_list(i + 1)
                 Case "Contact Number:"
                     data.Cellphone_no = new_list(i + 1)
                 Case "Make:"
                     data.make = new_list(i + 1)
                 Case "Varient:"
                     data.model = new_list(i + 1)
                 Case "Year:"
                     data.year = new_list(i + 1)
                 Case "Selling price:"
                     data.price = new_list(i + 1).Replace("R","")
                 case "Colour:"
                     data.colour = new_list(i + 1)
                 case "Mileage:"
                     data.mileage = new_list(i + 1)
                 case "Message:"
                     data.comment = new_list(i + 1)
                 case "dealer_id:"
                     if new_list(i + 1).ToString() = "null" Then
                         data.dealer_id = 2992
                     Else 
                         data.dealer_id = new_list(i + 1)
                     End If
                 case "lead_source:"
                     data.lead_source = new_list(i + 1)
                 case "referral_url:"
                     data.referral_url = new_list(i + 1)
                 case "stock_code:"
                     data.stock_code = new_list(i + 1)
                 case "Additional comments:"
                    Try
                        data.comment = new_list(i + 1)
                    Catch ex As Exception
                        data.comment = ""
                    End Try
                     
             End Select

         Next

        Console.WriteLine(JsonConvert.SerializeObject(data))

    End Sub

     Public Function process_data(data As BPFMC_JSON_data) As String
        Dim result As String = ""
        Dim my_token As String = GetToken
        result = send_data_API(data, data.dealer_id, my_token)
         
         Return result
     End Function
End Module

Public Class BPFMC_JSON_data
    Public Name As String 
    Public Surname As String
    Public Cellphone_no As String
    Public Email As String
    Public comment As String

    Public make As String
    Public model As String
    Public mileage As String
    Public year As String
    public price as String
    public dealer_id as integer
    public lead_source as string
    public stock_code as string
    public referral_url as string
    public colour as string
End Class