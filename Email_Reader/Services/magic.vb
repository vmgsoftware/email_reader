﻿Imports System.Data.SqlClient
Imports Dapper
Imports Email_Reader.Services
Imports HtmlAgilityPack
Imports MailKit
Imports MailKit.Net.Imap
Imports MailKit.Search
Imports MailKit.Security

Public Module magic
    ' Dim connection As String = "data source=127.0.0.1,50648;initial catalog=email_reader;persist security info=False;packet size=4096;user id = email_user;password = :WXQ_DlxO=e}"
    Dim connection As String = "data source=db4.vmgsoftware.co.za,50648;initial catalog=email_reader;persist security info=False;packet size=4096;user id = email_user;password = :WXQ_DlxO=e}"
    Public TxtEmail As String = "mail.vmgsoftware.co.za"
    Public TxtUsername As String = "emailreader@vmgsoftware.co.za"
    Public TxtPassword As String = "ZN5eFaQjKnVS"
    Sub tick_tock
        WriteErrorLog("Tick Tock started")
        Dim result = ""
        Dim output = ""
        Dim htmlStr = ""
        dim notProcessable as ImapFolder 
        Dim imap As New ImapClient()
        dim IX as ImapFolder  'used for the IX website leads
        dim BPFMC as ImapFolder  'used for the BPFMC Leads
        dim FacebookMailFolder As IMailFolder
        Dim subject = ""

        Try
             imap.Connect(TxtEmail, 143, SecureSocketOptions.None)
        'authenticating account with host
            imap.Authenticate(TxtUsername, TxtPassword)

        'check if connected
            If imap.IsConnected Then
                'check if authenticated
                If imap.IsAuthenticated Then

'                    CreateImapFolderIfNotExists("not_processable", imap)
                    notProcessable = imap.GetFolder("INBOX.not_processable")
'                    CreateImapFolderIfNotExists("Website", imap)
                    IX = imap.GetFolder("INBOX.IX_Website")
'                    CreateImapFolderIfNotExists("BPFMC", imap)
                    BPFMC = imap.GetFolder("INBOX.BPFMC")
                    CreateImapFolderIfNotExists("Facebook", imap)
                    FacebookMailFolder = imap.GetFolder("INBOX.Facebook")

                    Dim myInbox = imap.Inbox
                    myInbox.Open(FolderAccess.ReadWrite)
                    Dim unreadMails As Integer = myInbox.Unread
                    output = $"Total Messages: {unreadMails}" & vbCrLf
                    output &= $"Recent Messages: {myInbox.Recent}" & vbCrLf & vbCrLf

                    For Each uid In myInbox.Search(SearchQuery.NotSeen)
                        result = ""
                        Dim message = imap.Inbox.GetMessage(uid)
                        

                        output = message.HtmlBody
                        htmlStr = message.HtmlBody

                        subject = message.Subject.ToString()

                        If subject.Contains("iX Copy Lead") Then
                            
                            dim doc = new HtmlDocument()
                            doc.LoadHtml(htmlStr)
                    
                            Dim root = doc.DocumentNode
                            Dim a_nodes as List(Of HtmlNode) = root.Descendants("td").ToList()
                            dim ix_data as New IxJsonData
                            clean_text_IX(root.Descendants("td").ToList(),ix_data)
                            If subject.Contains("F and I - Website Dealer") Then
                                result = forward_email(message, getEmailAddress(ix_data, connection),1)
                                if result = "Success" then 
                                    imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                    imap.Inbox.MoveTo(uid,IX)
                                end if
                            ElseIf subject.Contains("HOW CAN WE HELP?") Then
                                result = forward_email(message, getEmailAddress(ix_data, connection),2)
                                if result = "Success" then 
                                    imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                    imap.Inbox.MoveTo(uid,IX)
                                end if
                            ElseIf subject.Contains("Sell Your Car") Then
                                result = forward_email(message, getEmailAddress(ix_data, connection),3)
                                if result = "Success" then 
                                    imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                    imap.Inbox.MoveTo(uid,IX)
                                end if
                            else
                                if ix_data.lead_ref = "Website" Then
                                    if ix_data.dealer <> "" then
                                        result = IX_Functions.process_data(ix_data,connection)
                    
                                        if result = "Created" then 
                                            imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                            imap.Inbox.MoveTo(uid,IX)
                                        end if
                                    else
                                        imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                        imap.Inbox.MoveTo(uid,notProcessable)
                                    End If
                                else
                                    imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                    imap.Inbox.MoveTo(uid,notProcessable)
                                End If
                            end if
                        ElseIf subject.Contains("Message from Best Price For My Car Website") Then
                            dim doc = new HtmlDocument()
                            doc.LoadHtml(htmlStr)
                    
                            Dim root = doc.DocumentNode
                            Dim a_nodes as List(Of HtmlNode) = root.Descendants("td").ToList()
                            dim BPFMC_data as New BPFMC_JSON_data
                            clean_text_BPFMC(root.Descendants("td").ToList(),BPFMC_data) 

                            if BPFMC_data.dealer_id <> 0 Then
                                result = BPFMC_Functions.process_data(BPFMC_data)
                    
                                if result = "Created" then 
                                    imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                                    imap.Inbox.MoveTo(uid,BPFMC)
                                end if
                            End If
                        Else
                            WriteErrorLog("Subject : " & message.Subject.ToString().Trim())
                            imap.Inbox.AddFlags(uid,MessageFlags.Seen, true)
                            imap.Inbox.MoveTo(uid,notProcessable)
                        end if
                    Next
                End If
                imap.Disconnect(True)
                RunAutoTraderLeads()
            End If
            WriteErrorLog("Tick Tock ended")
        Catch ex As Exception
            WriteErrorLog(ex)
        End Try
        'connecting to host
       
    End Sub

    public Sub CreateImapFolderIfNotExists(folderName As String, mailBox As ImapClient)
        For Each mailFolder As IMailFolder In mailBox.GetFolders(mailBox.PersonalNamespaces(0))
            If folderName.Equals(mailFolder.Name, StringComparison.InvariantCultureIgnoreCase)
                return
            End If
        Next

        Dim inboxMail = mailBox.GetFolder(mailBox.PersonalNamespaces(0))
        inboxMail.Create(folderName, False)
    End Sub


    Public Sub RunAutoTraderLeads()
        Try
            Dim db = New AutoTraderLeadsBackupDatabase(connection)

            dim lastUpdated = db.GetLastUpdatedDate().AddDays(-1)
            dim currentTime = DateTime.Now

            dim a = new AutoTraderLeadsApi(new Uri("https://services.autotrader.co.za/api/lead/v1.0"),
                                           "vmg@autotrader.co.za",
                                           "PsOzhfY72ZLKYkI8nHjI")

            dim d = a.DownloadLeads(lastUpdated, currentTime)

            If (d.Any()) Then
                Console.WriteLine($"Downloaded {d.Count} leads from {lastUpdated} to {currentTime}.")
                WriteErrorLog($"Downloaded {d.Count} leads from {lastUpdated} to {currentTime}.")
                db.SaveAutoTraderLeads(d)
            Else
                Console.WriteLine($"Downloaded 0 leads from {lastUpdated} to {currentTime}.")
                WriteErrorLog($"Downloaded 0 leads from {lastUpdated} to {currentTime}.")
            End If

            db.SetLastUpdatedDate(currentTime)

            Dim leads = db.GetLeadsForProcessing()

            Console.WriteLine($"{leads.Count} waiting for processing")
            WriteErrorLog($"{leads.Count} waiting for processing")

            For Each lead As AutoTraderLead In leads
                Try
                    Dim leadJson = lead.ToJsonData()
                    Dim result = ProcessDataAutoTraderApi(leadJson)
                    If result = "Created" Then
                        db.SetLeadStatus(lead.Id, AutoTraderLeadStatus.Processed)
                        db.WriteDbLog(lead.Id, "PROCESSED")
                    End If
                    WriteErrorLog(result & " : " & lead.Id)
                Catch ex As Exception
                    db.SetLeadStatus(lead.Id, AutoTraderLeadStatus.Failed)
                    db.WriteDbLog(lead.Id, "FAILED")
                End Try
            Next
        Catch ex As Exception
            WriteErrorLog($"{ex.StackTrace}")
            WriteErrorLog($"{ex.Message}")
        End Try
        
    End Sub

    Function ProcessDataAutoTraderApi(data As JsonData) As String
        Dim result = ""
        Dim compid = 0
        Try
            Using ctx = New SqlConnection(connection)
                Dim companyId = ctx.QuerySingleOrDefault(Of Integer?)("select company_id from tbldid where deleted = 0 and manageid = @did", New With {.did = data.did})

                If companyId IsNot Nothing AndAlso companyId > 0
                    Dim myToken = GetToken
                    result = send_data_API(data, compid, myToken)
                End If
            End Using
        Catch ex As Exception
            WriteErrorLog(ex)
        End Try
        
        Return result
    End Function
End Module

Public Class JsonDataAutoTrader2
    Public Name As String 
    Public Cellphone_no As String
    Public Email As String
    Public comment As String
    public price as string
    public did As Integer
    public reference as string
    public companyName as String
    Property vehicle As String
End Class

Public Class JsonData
    Public Name As String 
    Public Cellphone_no As String
    Public Email As String
    Public comment As String
    Public make As String
    Public model As String
    Public mileage As String
    Public year As String
    Public reg As String
    public price as string
    public did As Integer
    public reference as string

End Class
Public Class IxJsonData
    Public Name As String 
    Public Cellphone_no As String
    Public Email As String
    Public comment As String

    Public make As String
    Public model As String
    Public mileage As String
    Public year As String
    Public reg As String
    public price as String
    public iX_Lead_Ref  As string
    Public dealer as string
    public lead_source as string
    public lead_ref as string
    public stock_code as string
End Class

