﻿Imports System.IO
Imports System.Net.Mail
Imports Email_Reader.Utilities

Module LogFile
    Public Sub WriteErrorLog(ByVal ex As Exception)
        Dim sw As StreamWriter = Nothing
        Dim date_folder As String = Format(Now(), "yyyyMMdd")
        Try
            Dim logFilePath As String = AppDomain.CurrentDomain.BaseDirectory & "\" & date_folder
            If Directory.Exists(logFilePath) = False Then
                Directory.CreateDirectory(logFilePath)
            End If

            logFilePath += "\LogFile.log"

            If File.Exists(logFilePath) = False Then
                File.Create(logFilePath)
            End If


            Dim fileStream As FileStream = New FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
            Dim streamWriter As StreamWriter = New StreamWriter(fileStream)
            streamWriter.Close()
            fileStream.Close()

            Dim fileStream1 As FileStream = New FileStream(logFilePath, FileMode.Append, FileAccess.Write)
            sw = New StreamWriter(fileStream1)

            sw.WriteLine(DateTime.Now.ToString() & "--> " + ex.Source.ToString().Trim() & "--> " & ex.Message.ToString().Trim())
            sw.Flush()
            sw.Close()

            Dim mail As New MailMessage
            mail.Subject = "Email_Reader Error"
            mail.IsBodyHtml = True

            mail.Body = "Source: " + ex.Source.ToString().Trim() & vbCrLf & _
                        "Message: " +   ex.Message.ToString().Trim()  & vbCrLf & _
                        "StackTrace: " + ex.StackTrace.ToString.Trim()

            EmailService.SendMail(mail)
            sw.Dispose()
            fileStream1.Dispose()
        Catch
        End Try
    End Sub

    Public Sub WriteErrorLog(message As String)
        Dim sw As StreamWriter = Nothing
        Dim date_folder As String = Format(Now(), "yyyyMMdd")
        Try
            Dim logFilePath As String = AppDomain.CurrentDomain.BaseDirectory & "\" & date_folder
            If Directory.Exists(logFilePath) = False Then
                Directory.CreateDirectory(logFilePath)
            End If

            logFilePath += "\LogFile.log"

            If File.Exists(logFilePath) = False Then
                File.Create(logFilePath)
            End If


            Dim fileStream As FileStream = New FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
            Dim streamWriter As StreamWriter = New StreamWriter(fileStream)
            streamWriter.Close()
            fileStream.Close()

            Dim fileStream1 As FileStream = New FileStream(logFilePath, FileMode.Append, FileAccess.Write)
            sw = New StreamWriter(fileStream1)

            sw.WriteLine(DateTime.Now.ToString() & "--> " + message)
            sw.Flush()
            sw.Close()

            fileStream1.Close()
            sw.Dispose()
            fileStream1.Dispose()
        Catch ex As Exception
            WriteErrorLog(ex)
        End Try
    End Sub
End Module
